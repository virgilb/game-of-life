const create2DArray = (width, height, resolution) => {
  return new Array(
    Math.floor(height / resolution)
  )
    .fill(
      new Array(Math.floor(width / resolution))
      .fill(0)
    );
}

const step = (cells) => {
  cells.forEach((row, rowIdx) => {
    row.forEach((cell, cellIdx) => {

    });
  })
}

export default function sketch(s) {
  const width = s.windowWidth;
  const height = s.windowHeight;
  const resolution = 40;

  const colors = {
    'dead': '#232222',
    'alive': '#09C9B1',
  }

  let cells = create2DArray(width, height, resolution);


  // cells.forEach((row, rowIdx) => {
  //   row.forEach((cell, cellIdx) => {
  //     const val =
  //
  //   });
  // })
  s.setup = () => {
    s.createCanvas(s.windowWidth, s.windowHeight);

    for(let i = 0; i < height / resolution - 1; i++) {
      console.log('i', i);
      for(let j = 0; j < width / resolution - 1; j++) {
        console.log('j', j);
        cells[i][j] = s.random(2);
      }
    }

    console.table(cells);

    s.frameRate(5);
  };

  s.draw = () => {
    s.background(0, 0, 0)
    cells.forEach((row, y) => {
      row.forEach((cell, x) => {
          s.rect(
            x * resolution ,
            y * resolution,
            resolution,
            resolution
          );
          s.fill(cell === 0 ? colors.dead : colors.alive);
      })
    })
  };

  s.mousePressed = () => {
    console.log('step');
    step(cells);
  };

  s.windowResized = () => {
    s.resizeCanvas(s.windowWidth, s.windowHeight);
    cells = create2DArray(s.windowWidth, s.windowHeight, resolution);
  }
}
